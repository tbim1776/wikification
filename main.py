import nltk
import pandas as pd
import xml.etree.ElementTree as et
import math
import random
import re

from collections import Counter, OrderedDict
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

NUMBER_OF_TEST = 200
NUMBER_OF_ARTICLES = 200
articles = []
tests = []
path = "name.xml"

stop_words = set(stopwords.words('english'))
tk = RegexpTokenizer(r'\w+')

context = et.iterparse(path, events=("start", "end"))
context = iter(context)
ev, root = next(context)

keyphs = dict()
keyws2 = dict()
phrs = []

for ev, el in context:
    if 'text' in el.tag:
        s = str(el.text).lower()
        if (len(s) > 2000) and (len(articles) < NUMBER_OF_ARTICLES):
            for phrase in re.findall(r'\[\[(.*?)]]', s):
                ll = []
                for word in tk.tokenize(phrase):
                    if (word not in stop_words) and (not str(word).isnumeric()):
                        ll.append(word)
                phrs.append(ll)
            articles.append(tk.tokenize(s))
        elif len(s) > 2000:
            tests.append(tk.tokenize(s))
            if len(tests) == NUMBER_OF_TEST:
                break
        root.clear()

no_of_articles_of_words = dict()

for i in range(len(articles)):
    for word in articles[i]:
        if (word not in stop_words) and (word not in no_of_articles_of_words) and (not str(word).isnumeric()):
            no_of_articles_of_words[word] = 1
            for j in range(i+1, len(articles)):
                if word in articles[j]:
                    no_of_articles_of_words[word] = no_of_articles_of_words[word] + 1

for i in range(len(phrs)):
    phrase = phrs[i]
    for word in phrase:
        if (word not in stop_words) and (not str(word).isnumeric()):
            if word not in keyws2:
                keyws2[word] = 1
                for j in range(i+1, len(phrs)):
                    if word in phrs[j]:
                        keyws2[word] = keyws2[word] + 1

keyphraseness = dict()

for key in keyws2:
    keyphraseness[key] = keyws2[key] / no_of_articles_of_words[key]

keyphraseness_sorted = OrderedDict(sorted(keyphraseness.items(), key=lambda item: item[1], reverse=True))

print("counted the words!")

list_of_tfidf = []
list_of_tf = []
# TF-IDF

for i in range(NUMBER_OF_ARTICLES):
    counts = Counter(articles[i])
    tf_values = dict(counts)
    tfidf_values = dict()
    for key in counts:
        if key in no_of_articles_of_words:
            tfidf_values[key] = counts[key] * math.log(NUMBER_OF_ARTICLES/no_of_articles_of_words[key])
    list_of_tfidf.append(OrderedDict(sorted(tfidf_values.items(), key=lambda item: item[1], reverse=True)))
    list_of_tf.append(OrderedDict(sorted(tf_values.items(), key=lambda item: item[1], reverse=True)))
    # d = pd.DataFrame.from_dict(tf_values, orient="index")
    # d.to_csv('tfidf/tf'+str(i)+'.csv')

for i in range(NUMBER_OF_ARTICLES):
    text_file = open('tfidf/keywords_from_doc_no' + str(i) + '.txt', "w")
    for j in range(int(len(list_of_tfidf[i])*0.06)):
        elem = list(list_of_tfidf[i].keys())[j]
        text_file.write(elem + '\n')
    text_file.close()

print("Keywords saved with the tf-idf method")

# KEYPHRASENESS

for i in range(len(tests)):
    article = tests[i]
    keywords = []
    for key, value in keyphraseness_sorted.items():
        if key in article:
            # r = random.random()
            # if r < keyphraseness_sorted[key]:
            keywords.append(key)
        if len(keywords) >= int(len(article)*0.06):
            break
    text_file = open('keyphraseness/keywords_from_testdoc_no' + str(i) + '.txt', "w")
    for elem in keywords:
        text_file.write(elem + '\n')
    text_file.close()

print("Keywords saved with the keyphraseness method")

#keywords = dict()

#for i in range(NUMBER_OF_ARTICLES):
#    for j in range(int(len(list_of_tfidf[i])*0.06)):
#        elem = list(list_of_tfidf[i].keys())[j]
#        if elem in keywords:
#            keywords[elem] = keywords[elem] + 1
#        else:
#            keywords[elem] = 1

#keywords_from_docs = []

#for i in range(NUMBER_OF_ARTICLES):
#    res = []
#    for j in range(len(articles[i])):
#        word = articles[i][j]
#        if word in keywords:
#            ratio = keywords[word] / list_of_tfidf[i][word]
#            r = random.random()
#            if (r < ratio) and (word not in res):
#                res.append(word)
#    if res:
#        text_file = open('keyphraseness/keyphrases'+str(i)+'.txt', "w")
#        keywords_from_docs.append(res)
#        for w in res:
#            text_file.write(w + '\n')
#        text_file.close()

